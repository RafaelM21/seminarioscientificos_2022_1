package br.com.mauda.seminario.cientificos.model;

import java.time.LocalDateTime;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao {

    private Long id;
    private Boolean direitoMaterial;
    private LocalDateTime dataCriacao;
    private LocalDateTime dataCompra;
    private LocalDateTime dataCheckIn;
    private Seminario seminario;
    private Estudante estudante;
    private SituacaoInscricaoEnum situacao;

    public Inscricao(Seminario seminario) {
        this.seminario = seminario;
        this.seminario.adicionarInscricao(this);
        this.dataCriacao = LocalDateTime.now();
        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;

    }

    public void comprar(Estudante estudante, boolean direitoMaterial) {

        this.estudante = estudante;
        this.direitoMaterial = direitoMaterial;
        this.estudante.adicionarInscricao(this);
        this.dataCompra = LocalDateTime.now();
        this.situacao = SituacaoInscricaoEnum.COMPRADO;

    }

    public void cancelarCompra() {

        this.estudante.removerInscricao(this);
        this.estudante = null;
        this.direitoMaterial = null;
        this.dataCompra = null;
        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;

    }

    public void realizarCheckIn() {
        this.dataCheckIn = LocalDateTime.now();
        this.situacao = SituacaoInscricaoEnum.CHECKIN;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDataCriacao() {
        return this.dataCriacao;
    }

    public LocalDateTime getDataCompra() {
        return this.dataCompra;
    }

    public LocalDateTime getDataCheckIn() {
        return this.dataCheckIn;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Inscricao other = (Inscricao) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacao;
    }

}
