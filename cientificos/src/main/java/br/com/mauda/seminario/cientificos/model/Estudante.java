package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Estudante {

    private Long id;
    private String nome;
    private String telefone;
    private String email;
    private Instituicao instituicao;
    private List<Inscricao> inscricoes = new ArrayList<>();

    public Estudante(Instituicao instituicao) {
        this.instituicao = instituicao;
    }

    public void adicionarInscricao(Inscricao inscricao) {
        this.inscricoes.add(inscricao);
    }

    public boolean possuiInscricao(Inscricao inscricao) {
        return this.inscricoes.contains(inscricao);
    }

    public void removerInscricao(Inscricao inscricao) {
        this.inscricoes.remove(inscricao);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Estudante other = (Estudante) obj;
        return this.id == other.id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Instituicao getInstituicao() {
        return this.instituicao;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

}
