package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Professor {

    private Long id;
    private String email;
    private String nome;
    private Double salario;
    private String telefone;
    private Instituicao instituicao;
    private List<Seminario> seminarios = new ArrayList<>();

    public Professor(Instituicao instituicao) {
        this.instituicao = instituicao;
    }

    public void adicionarSeminario(Seminario seminarios) {
        this.seminarios.add(seminarios);
    }

    public boolean possuiSeminario(Seminario seminarios) {
        return this.seminarios.contains(seminarios);
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getSalario() {
        return this.salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Instituicao getInstituicao() {
        return this.instituicao;
    }

    public List<Seminario> getSeminarios() {
        return this.seminarios;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Professor other = (Professor) obj;
        return Objects.equals(this.id, other.id);
    }

}
